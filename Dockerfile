FROM ubuntu:18.04

# Disable spam
ENV SUPPRESS_SUPPORT=true \
    OPENCOLLECTIVE_HIDE=true \
    DISABLE_OPENCOLLECTIVE=true \
    NPM_CONFIG_FUND=false

ENV CHROME_BIN=/usr/bin/chromium-browser \
    CHROME_PATH=/usr/lib/chromium/ \
    PATH=/home/node/.npm-global/bin:$PATH \
    NPM_CONFIG_PREFIX=/home/node/.npm-global \
    DEBIAN_FRONTEND=noninteractive \
    DEBCONF_NONINTERACTIVE_SEEN=true \
    JDK_VERSION=11

RUN apt-get update && apt-get install -y \
bash \
curl \
software-properties-common

SHELL ["/bin/bash", "-c" ]

# Apt repositories & keys
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-key adv --fetch-keys "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xea6e302dc78cc4b087cfc3570ebea9b02842f111" \
    && echo 'deb http://ppa.launchpad.net/chromium-team/beta/ubuntu bionic main ' \
    && add-apt-repository ppa:openjdk-r/ppa

# Apt
RUN apt-get update && apt-get install -y \
git \
jq \
nodejs=14.15.1-1nodesource1 \
python-pip \
libtool \
python3 \
build-essential \
make \
autoconf \
automake \
chromium-browser \
openjdk-${JDK_VERSION}-jdk \
unzip \
qt5-default \
wget

# Nodejs setup
USER root
RUN mkdir -p /home/node/.npm-global
RUN useradd node
RUN chown -R node:node /home/node

USER node

# NPM
RUN npm i -g \
typescript \
yarn \
expo-cli \
apkup

USER root

# Gradle
ENV GRADLE_VERSION=6.6.1
ENV GRADLE_HOME=/opt/gradle/gradle-$GRADLE_VERSION
ENV PATH=$PATH:$GRADLE_HOME/bin
RUN mkdir -p /opt/gradle \
    && curl -L https://services.gradle.org/distributions/gradle-$GRADLE_VERSION-bin.zip -o gradle-$GRADLE_VERSION-bin.zip \
    && unzip gradle-$GRADLE_VERSION-bin.zip -d /opt/gradle/ \
    && rm gradle-$GRADLE_VERSION-bin.zip

# react-native
ENV ADB_IP="192.168.1.1"
ENV REACT_NATIVE_PACKAGER_HOSTNAME="192.255.255.255"

# Android SDK
# download and install Kotlin compiler
# https://github.com/JetBrains/kotlin/releases/latest
ARG KOTLIN_VERSION=1.4.10
RUN cd /opt && \
    wget -q https://github.com/JetBrains/kotlin/releases/download/v${KOTLIN_VERSION}/kotlin-compiler-${KOTLIN_VERSION}.zip && \
    unzip *kotlin*.zip && \
    rm *kotlin*.zip

# download and install Android SDK
# https://developer.android.com/studio#command-tools
ARG ANDROID_SDK_VERSION=6858069
ENV ANDROID_SDK_ROOT /opt/android-sdk
RUN mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools && \
    wget -q https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_VERSION}_latest.zip && \
    unzip *tools*linux*.zip -d ${ANDROID_SDK_ROOT}/cmdline-tools && \
    mv ${ANDROID_SDK_ROOT}/cmdline-tools/cmdline-tools ${ANDROID_SDK_ROOT}/cmdline-tools/tools && \
    rm *tools*linux*.zip

# set the environment variables
ENV JAVA_HOME /usr/lib/jvm/java-${JDK_VERSION}-openjdk-amd64
ENV GRADLE_HOME /opt/gradle
ENV KOTLIN_HOME /opt/kotlinc
ENV PATH ${PATH}:${GRADLE_HOME}/bin:${KOTLIN_HOME}/bin:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin:${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin:${ANDROID_SDK_ROOT}/platform-tools:${ANDROID_SDK_ROOT}/emulator
# WORKAROUND: for issue https://issuetracker.google.com/issues/37137213
ENV LD_LIBRARY_PATH ${ANDROID_SDK_ROOT}/emulator/lib64:${ANDROID_SDK_ROOT}/emulator/lib64/qt/lib
# patch emulator issue: Running as root without --no-sandbox is not supported. See https://crbug.com/638180.
# https://doc.qt.io/qt-5/qtwebengine-platform-notes.html#sandboxing-support
ENV QTWEBENGINE_DISABLE_SANDBOX 1

# accept the license agreements of the SDK components
COPY license_accepter.sh /opt/
RUN chmod +x /opt/license_accepter.sh && /opt/license_accepter.sh $ANDROID_SDK_ROOT

# setup adb server
EXPOSE 5037
EXPOSE 19000
EXPOSE 19001

RUN pip install awscli

RUN apt-get update && \
    apt-get install -y android-tools-adb
WORKDIR /app

SHELL ["/bin/bash", "-c"]